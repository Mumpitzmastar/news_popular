<?php

if( ! defined( 'TYPO3_MODE' ) ){
	die( 'Access denied.' );
}

TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns( 'tx_news_domain_model_news', array(
	'tx_newspopular_views' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:news_popular/Resources/Private/Language/locallang_db.xlf:tx_news_domain_model_news.tx_newspopular_views',
		'config' => array(
			'type' => 'input',
			'size' => 10,
			'eval' => 'trim',
			'readOnly' => true
		)
	)
));
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes( 'tx_news_domain_model_news' , 'tx_newspopular_views' , '' , '');	

?>