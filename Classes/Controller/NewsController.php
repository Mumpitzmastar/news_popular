<?php

namespace DominikWeber\NewsPopular\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dominik Weber <post@dominikweber.de>, www.dominikweber.de
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * News Controller for most read news items
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class NewsController extends AbstractController{

	/**
	 * widgetAction
	 * 
	 * @return view
	 */
	public function widgetAction(){
		switch( $this->settings['mode'] ){
			case 1:
				$start = $this->settings['start'];
				$stop = $this->settings['stop'];
			break;
			case 2:
				if( $this->settings['timeFrame'] == 'custom' ){
					$start = strtotime( $this->settings['customTimeFrame'] );
				}
				else{
					$start = strtotime( $this->settings['timeFrame'] );
				}
				$stop = time();
			break;
		}
		$news = $this->newsRepository->findMostViewed(
			$this->settings['maxItems'],
			$this->categories,
			$this->settings['categoryConstraint'],
			$start,
			$stop
		);
		$this->view->assign( 'news' , $news );
	}

}