<?php

namespace DominikWeber\NewsPopular\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Dominik Weber <post@dominikweber.de>, www.dominikweber.de
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class NewsRepository extends \GeorgRinger\News\Domain\Repository\NewsRepository{

	/**
	 * findMostViewed
	 * 
	 * @param  integer $limit max items
	 * @param  array $categories
	 * @param  string $categoryConstraint
	 * @param  integer $start start timestamp
	 * @param  integer $stop stop timestamp
	 * @return results
	 */
	public function findMostViewed( $limit=10 , $categories=NULL , $categoryConstraint=NULL , $start=NULL , $stop=NULL ){
		$query = $this->createQuery();
		if( $start && $stop ){
			$constraints[] = $query->greaterThanOrEqual( 'datetime' , $start );
			$constraints[] = $query->lessThanOrEqual( 'datetime' , $stop );
		}
		if( $categories ){
			$categoryConstraint = ! empty( $categoryConstraint ) ? $categoryConstraint : 'OR';
			foreach( $categories as $category ){
				$categoryConstraints[] = $query->contains( 'categories' , $category->getUid() );
			}
			if( $categoryConstraint === 'OR' ){
				$constraints[] = $query->logicalOr( $categoryConstraints );
			}
			else if( $categoryConstraint === 'AND' ){
				$constraints[] = $query->logicalAnd( $categoryConstraints );
			}
		}
		if( $constraints ){
			$query->matching(
				$query->logicalAnd( $constraints )
			);
		}
		$query->setOrderings(
			array( 'views' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING )
		);
		$query->setLimit( (int) $limit );
		return $query->execute();
	}

}